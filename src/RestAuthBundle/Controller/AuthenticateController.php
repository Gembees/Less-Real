<?php

namespace RestAuthBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

class AuthenticateController extends Controller
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getToken(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $userReceived = new User();
        $userReceived
            ->setUsername($data['username'])
            ->setPassword($data['password'])
            ->encodePassword();

        $user = $this->entityManager
            ->getRepository('UserBundle:User')
            ->findOneBy([
                'username' => $userReceived->getUsername(),
                'password' => $userReceived->getPassword(),
            ]);

        if (!$user) {
            throw $this->createNotFoundException('Wrong credentials.');
        }

        //generate token
        $token = md5(time().'Z'.$user->getUsername().'GGDAga548465483graGARG541321.G');
        $user->setToken($token);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $response = new Response(json_encode(['token' => $token]), 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function authToken($token)
    {
        $user = $this->getDoctrine()
            ->getRepository('UserBundle:User')
            ->findOneBy([
                'token' => $token
            ]);

        if (!$user)
        {
            return false;
        }

        return true;
    }
}
