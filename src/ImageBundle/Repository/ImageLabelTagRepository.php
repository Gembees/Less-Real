<?php

namespace ImageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ImageBundle\Entity\ImageLabelTag;

class ImageLabelTagRepository extends EntityRepository
{
    public function findOneByLabelIdAndImageId(int $labelId, int $imageId): ?ImageLabelTag
    {
        return $this->findOneBy([
            'label' => $labelId,
            'image' => $imageId
        ]);
    }
}
