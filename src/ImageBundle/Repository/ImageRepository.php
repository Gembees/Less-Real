<?php

namespace ImageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use ImageBundle\Entity\Image;
use UserBundle\Entity\User;

class ImageRepository extends EntityRepository
{
    public function findOneById(int $id): ?Image
    {
        return $this->find($id);
    }

    /**
     * @param User $user
     * @param int|null $n
     * @return array Image|null
     */
    public function getUnfinishedImageUploads(User $user, int $n = null): ?array
    {
        //TODO: actually fetch unfinished image uploads
        return $this->findBy([
            'status' => Image::STATUS_UPLOADED,
            'user' => $user
        ], null, $n);
    }

    /**
     * @param int $n
     * @return array Image|null
     */
    public function getNewestImages(int $n): ?array
    {
        //TODO: actually fetch unfinished image uploads
        return $this->createQueryBuilder('i')
            /*->where('i.width = i.height')*/
            ->orderBy('i.dateCreated', 'DESC')
            ->setMaxResults($n)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @param int $n
     * @return array Image|null
     */
    public function getUntaggedUnfinishedImageUploads(User $user, int $n): ?array
    {
        return $this->createQueryBuilder('i')
            ->select('i.id')
            ->leftJoin('ImageBundle:ImageLabelTag', 'lt', Join::LEFT_JOIN, 'lt.image = i.id')
            ->where('i.status = \'' . Image::STATUS_UPLOADED . '\'')
            ->andWhere('i.user = :user')
            ->andWhere('lt.id is null')
            ->setMaxResults($n)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function finishUnfinishedImageUploads()
    {
        $this->createQueryBuilder('i')
            ->update('ImageBundle:Image', 'i')
            ->set('i.status', ':statusActive')
            ->where('i.status = :statusUploaded')
            ->andWhere('i.user = :user')
            ->setParameter(':statusActive', Image::STATUS_ACTIVE)
            ->setParameter(':statusUploaded', Image::STATUS_UPLOADED)
            ->setParameter(':user', 4)
            ->getQuery()
            ->execute();
    }
}
