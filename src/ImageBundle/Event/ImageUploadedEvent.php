<?php

namespace ImageBundle\Event;

use ImageBundle\Entity\Image;
use Symfony\Component\EventDispatcher\Event;

class ImageUploadedEvent extends Event
{
    const NAME = 'image.uploaded';

    protected $image;

    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
