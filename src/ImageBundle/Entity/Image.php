<?php

namespace ImageBundle\Entity;

use UserBundle\Entity\User;

class Image
{
    /** image has been uploaded, but not finalized */
    public const STATUS_UPLOADED = 'uploaded';
    /** image is active and visible to users */
    public const STATUS_ACTIVE = 'active';
    /** image is hidden from the public */
    public const STATUS_HIDDEN = 'hidden';
    /** image has been deleted */
    public const STATUS_DELETED = 'deleted';
    /** image has just been created */
    public const STATUS_NEW = 'new';

    /** image height is bigger than width */
    public const DISPLAY_VERTICAL = 1;
    /** image width is bigger than height */
    public const DISPLAY_HORIZONTAL = 2;

    public const FILE_EXTENSION_JPG = 'jpg';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string
     */
    private $fileExtension;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * @var int
     */
    private $display;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $thumbnailWidth;

    /**
     * @var int
     */
    private $thumbnailHeight;

    /**
     * @var \DateTime
     */
    private $dateUpdated;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    public function __construct()
    {
        $this->status = self::STATUS_NEW;
        $this->dateUpdated = new \DateTime();
        $this->dateCreated = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPartition(): int
    {
        //TODO: have a global for images per partition
        $imagesPerPartition = 5000;
        return ceil($this->getId() / $imagesPerPartition);
    }

    public function getPath(): string
    {
        return "images/preupload/fixedThumbnails/" . $this->getFileName();
    }

    public function getDynamicThumbPath(): string
    {
        //TODO: actually return the thumbnail
        if (file_exists("images/" . $this->getFileName())) {
            return "images/" . $this->getFileName();
        } else {
            return "images/preupload/dynamicThumbnails/" . $this->getFileName();
        }
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Image
    {
        $this->status = $status;
        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): string
    {
        $this->source = $source;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Image
    {
        $this->user = $user;
        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): Image
    {
        $this->hash = $hash;
        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): Image
    {
        $this->fileName = $fileName;
        return $this;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): Image
    {
        $this->display = $display;
        return $this;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function setWidth(int $width): Image
    {
        $this->width = $width;
        return $this;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): Image
    {
        $this->height = $height;
        return $this;
    }

    public function getThumbnailWidth(): int
    {
        return $this->thumbnailWidth;
    }

    public function setThumbnailWidth(int $thumbWidth): Image
    {
        $this->thumbnailWidth = $thumbWidth;
        return $this;
    }

    public function getThumbnailHeight(): int
    {
        return $this->thumbnailHeight;
    }

    public function setThumbnailHeight(int $thumbHeight): Image
    {
        $this->thumbnailHeight = $thumbHeight;
        return $this;
    }

    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    public function setFileExtension(string $fileExtension): Image
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }

    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    public function setFileSize(int $fileSize): Image
    {
        $this->fileSize = $fileSize;
        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTime $dateUpdated): Image
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTime $dateCreated): Image
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }
}
