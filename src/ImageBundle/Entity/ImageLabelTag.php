<?php

namespace ImageBundle\Entity;

use LabelBundle\Entity\Label;
use UserBundle\Entity\User;

class ImageLabelTag
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Label
     */
    private $label;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var \DateTime
     */
    private $dateUpdated;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    public function __construct()
    {
        $this->dateCreated = new \DateTime();
        $this->dateUpdated = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): ImageLabelTag
    {
        $this->user = $user;
        return $this;
    }

    public function getLabel(): Label
    {
        return $this->label;
    }

    public function setLabel(Label $label): ImageLabelTag
    {
        $this->label = $label;
        return $this;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function setImage(Image $image): ImageLabelTag
    {
        $this->image = $image;
        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTime $dateUpdated): ImageLabelTag
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTime $dateCreated): ImageLabelTag
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }
}
