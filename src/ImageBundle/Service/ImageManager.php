<?php

namespace ImageBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use ImageBundle\Entity\Image;
use ImageBundle\Exception\ImageUploadException;
use ImageBundle\Repository\ImageRepository;
use Psr\Log\LoggerInterface;
use UserBundle\Service\CurrentUserProvider;

class ImageManager
{
    private $entityManager;
    private $currentUserProvider;
    private $imageRepository;
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        CurrentUserProvider $currentUserProvider,
        ImageRepository $imageRepository,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->currentUserProvider = $currentUserProvider;
        $this->imageRepository = $imageRepository;
        $this->logger = $logger;
    }

    public function finishImageUpload(): void
    {
        $this->logger->debug(__METHOD__);

        $images = $this->imageRepository->getUnfinishedImageUploads($this->currentUserProvider->getCurrent());
        if ($images === null) {
            throw new ImageUploadException("Couldn't find any unfinished image uploads for this user");
        }

        foreach ($images as $image) {
            $this->markImageAsActive($image);
        }
    }

    private function markImageAsActive(Image $image): void
    {
        $image->setStatus(Image::STATUS_ACTIVE);
    }
}