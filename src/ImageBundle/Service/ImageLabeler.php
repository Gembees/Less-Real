<?php

namespace ImageBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use ImageBundle\Entity\ImageLabelTag;
use ImageBundle\Repository\ImageLabelTagRepository;
use ImageBundle\Repository\ImageRepository;
use LabelBundle\Exception\LabelerException;
use LabelBundle\Repository\LabelRepository;
use LabelBundle\Service\LabelerInterface;
use Psr\Log\LoggerInterface;
use UserBundle\Service\CurrentUserProvider;

class ImageLabeler implements LabelerInterface
{
    private $currentUserProvider;
    private $labelRepository;
    private $imageRepository;
    private $imageLabelTagRepository;
    private $entityManager;
    private $logger;

    public function __construct(
        CurrentUserProvider $currentUserProvider,
        LabelRepository $labelRepository,
        ImageRepository $imageRepository,
        ImageLabelTagRepository $imageLabelTagRepository,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->currentUserProvider = $currentUserProvider;
        $this->labelRepository = $labelRepository;
        $this->imageRepository = $imageRepository;
        $this->imageLabelTagRepository = $imageLabelTagRepository;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function tagEntity(int $labelId, int $entityId): void
    {
        $this->logger->debug(__METHOD__);

        $label = $this->labelRepository->findOneById($labelId);
        if ($label === null) {
            throw new LabelerException("Could not find label with provided id.");
        }
        $image = $this->imageRepository->findOneById($entityId);
        if ($image === null) {
            throw new LabelerException('Could not find image with provided id.');
        }
        if ($this->isEntityTaggedWithLabel($labelId, $entityId) === true) {
            throw new LabelerException("Image already tagged with this label.");
        }

        $imageLabelTag = (new ImageLabelTag())
            ->setUser($this->currentUserProvider->getCurrent())
            ->setLabel($label)
            ->setImage($image)
        ;
        $this->entityManager->persist($imageLabelTag);
    }

    public function untagEntity(int $labelId, int $entityId): void
    {
        $this->logger->debug(__METHOD__);

        $label = $this->labelRepository->findOneById($labelId);
        if ($label === null) {
            throw new LabelerException("Could not find label with provided id.");
        }
        $image = $this->imageRepository->findOneById($entityId);
        if ($image === null) {
            throw new LabelerException('Could not find image with provided id.');
        }
        if ($this->isEntityTaggedWithLabel($labelId, $entityId) === false) {
            throw new LabelerException("Image not tagged with this label.");
        }

        $imageLabelTag = $this->imageLabelTagRepository->findOneByLabelIdAndImageId($labelId, $entityId);
        $this->entityManager->remove($imageLabelTag);
    }

    public function isEntityTaggedWithLabel(int $labelId, int $entityId): bool
    {
        $this->logger->debug(__METHOD__);

        if ($this->imageLabelTagRepository->findOneByLabelIdAndImageId($labelId, $entityId) === null) {
            return false;
        } else {
            return true;
        }
    }
}