<?php

namespace ImageBundle\Listener;

use AppBundle\Service\NodeJsClient;
use ImageBundle\Event\ImageUploadedEvent;
use Psr\Log\LoggerInterface;

class ImageListener
{
    private $logger;
    private $nodeClient;

    public function __construct(
        LoggerInterface $logger,
        NodeJsClient $nodeClient
    ) {
        $this->logger = $logger;
        $this->nodeClient = $nodeClient;
    }

    public function onImageUploaded(ImageUploadedEvent $event)
    {
        //TODO: remove the two debug lines
        $image = $event->getImage();
        $this->logger->debug('IMAGE UPLOADEEEEEEEEEEEEEEEEEEEEEED: ' . $image->getId());
        $this->nodeClient->dispatchEvent(ImageUploadedEvent::NAME, $event);
    }
}
