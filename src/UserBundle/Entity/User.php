<?php

namespace UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    public const GENDER_UNKNOWN = '0';
    public const GENDER_MALE = '1';
    public const GENDER_FEMALE = '2';
    public const GENDER_ATTACK_HELICOPTER = '3';

    public const DEFAULT_AVATAR_PATH = '/themes/default/assets/images/default_avatar.jpg';
    public const DEFAULT_COVER_PATH = '/themes/default/assets/images/default_cover.jpg';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $location;

    /**
     * @var integer
     */
    private $gender;

    /**
     * @var \DateTime
     */
    private $birthday;

    /**
     * @var string
     */
    private $introduction;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $token;

    /**
     * @var \DateTime
     */
    private $dateAvatarChanged;

    /**
     * @var \DateTime
     */
    private $dateCoverChanged;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \DateTime
     */
    private $dateUpdated;

    public function __construct()
    {
        $this->dateCreated = new \DateTime();
        $this->dateUpdated = new \DateTime();
        $this->dateAvatarChanged = new \DateTime();
        $this->dateCoverChanged = new \DateTime();
        $this->gender = self::GENDER_UNKNOWN;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getRoles(): array
    {
        return array('ROLE_USER');
    }

    public function getCurrentAvatarPath(): string
    {
        if (file_exists($this->getRealCurrentAvatarPath())) {
            return '/images/avatars/' . $this->getId()
                . '_avatar.jpg?t=' . $this->getDateAvatarChanged()->format('U');
        } else {
            return self::DEFAULT_AVATAR_PATH;
        }
    }

    public function getRealCurrentAvatarPath(): string
    {
        return __DIR__ . '/../../../web/images/avatars/' . $this->getId() . '_avatar.jpg';
    }

    public function getNewAvatarPath(): string
    {
        return '/images/avatars/' . $this->getId() . '_avatar_new.jpg';
    }

    public function getRealNewAvatarPath(): string
    {
        return __DIR__ . '/../../../web/images/avatars/' . $this->getId() . '_avatar_new.jpg';
    }

    public function getCurrentCoverPath(): string
    {
        if (file_exists($this->getRealCurrentCoverPath())) {
            return '/images/covers/' . $this->getId() . '_cover.jpg?t='
                . $this->getDateCoverChanged()->format('U');
        } else {
            return self::DEFAULT_COVER_PATH;
        }
    }

    public function getRealCurrentCoverPath(): string
    {
        return __DIR__ . '/../../../web/images/covers/' . $this->getId() . '_cover.jpg';
    }

    public function getNewCoverPath(): string
    {
        return '/images/covers/' . $this->getId() . '_cover_new.jpg';
    }

    public function getRealNewCoverPath(): string
    {
        return __DIR__ . '/../../../web/images/covers/' . $this->getId() . '_cover_new.jpg';
    }

    public function getFormattedBirthday(): string
    {
        return $this->getBirthday()->format('d - m - y');
    }

    public function getSalt(): string
    {
        return "kgA4aHTr64428rGFDZg8963";
    }

    public function encodePassword(): string
    {
        $this->setPassword(md5("Z".$this->password. $this->getSalt()));
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): User
    {
        $this->password = $password;
        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): User
    {
        $this->location = $location;
        return $this;
    }

    public function getGender(): int
    {
        return $this->gender;
    }

    public function setGender(int $gender): User
    {
        $this->gender = $gender;
        return $this;
    }

    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTime $birthday): User
    {
        $this->birthday = $birthday;
        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(?string $introduction): User
    {
        $this->introduction = $introduction;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): User
    {
        $this->token = $token;
        return $this;
    }

    public function getDateAvatarChanged(): \DateTime
    {
        return $this->dateAvatarChanged;
    }

    public function setDateAvatarChanged(\DateTime $dateAvatarChanged): User
    {
        $this->dateAvatarChanged = $dateAvatarChanged;
        return $this;
    }

    public function getDateCoverChanged(): \DateTime
    {
        return $this->dateCoverChanged;
    }

    public function setDateCoverChanged(\DateTime $dateCoverChanged): User
    {
        $this->dateCoverChanged = $dateCoverChanged;
        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTime $dateCreated): User
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTime $dateUpdated): User
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }
}
