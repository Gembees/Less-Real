<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;

class ProfileController extends Controller
{
    private $twig;
    private $formFactory;

    public function __construct(
        TwigEngine $twig,
        FormFactoryInterface $formFactory
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
    }

    public function front()
    {
        $form = $this->formFactory->createBuilder()
            ->add('username', TextType::class, [
                'attr' => [
                    'placeholder' => 'Username',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'Password'
                ]
            ])
            ->add('submit', SubmitType::class)
            ->getForm();


        return $this->twig->renderResponse('UserBundle:Profile:profile.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
