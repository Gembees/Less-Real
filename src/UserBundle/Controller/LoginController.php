<?php

namespace UserBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use UserBundle\Entity\User;
use UserBundle\Exception\UserRegisterException;
use UserBundle\Service\UserManager;
use UserBundle\Service\UserProvider;

class LoginController extends Controller
{
    const ERROR_MESSAGE_INVALID_CREDENTIALS = 'Invalid credentials.';

    private $twig;
    private $formFactory;
    private $authenticationUtils;
    private $entityManager;
    private $userProvider;
    private $userManager;
    private $tokenStorage;
    private $logger;

    public function __construct(
        TwigEngine $twig,
        FormFactoryInterface $formFactory,
        AuthenticationUtils $authenticationUtils,
        EntityManagerInterface $entityManager,
        UserProvider $userProvider,
        UserManager $userManager,
        TokenStorage $tokenStorage,
        LoggerInterface $logger
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->authenticationUtils = $authenticationUtils;
        $this->entityManager = $entityManager;
        $this->userProvider = $userProvider;
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
    }

    public function loginPlease()
    {
        return $this->twig->renderResponse('UserBundle:Login:loginPlease.html.twig');
    }

    public function check()
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();

        if ($error !== null) {
            return new Response(json_encode(['success' => false, 'error' => self::ERROR_MESSAGE_INVALID_CREDENTIALS]), 401);
        } else {
            return new Response(json_encode(['success' => true]), 200);
        }
    }

    public function login()
    {
        $form = $this->formFactory->createBuilder()
            ->add('username', TextType::class, [
                'attr' => [
                    'placeholder' => 'Username',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'Password'
                ]
            ])
            ->add('submit', SubmitType::class)
            ->getForm();


        return $this->twig->renderResponse('UserBundle:Login:login.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param string $name
     * @return string
     */
    private function parseFacebookName(string $name)
    {
        $name = str_replace(" ", '', trim($name));
        preg_replace("/[^A-Za-z0-9 ]/", '', $name);

        return $name;
    }

    public function loginFacebook()
    {
        $fb = new Facebook([
            'app_id' => '1100151823443723',
            'app_secret' => 'af28290107b3bd10993aad6f47051f55',
            'default_graph_version' => 'v2.8',
        ]);

        $helper = $fb->getJavaScriptHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (FacebookResponseException $e) {
            // When Graph returns an error
            $this->logger->warning('Facebook graph returned an error', ['error' => $e->getMessage()]);
            exit;
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            $this->logger->warning('Facebook SDK returned an error', ['error' => $e->getMessage()]);
            exit;
        }

        if (!isset($accessToken)) {
            $this->logger->debug('No cookie set or no OAuth data could be obtained from cookie.');
            exit;
        }

        $response = $fb->get('/me?fields=email,first_name', $accessToken->getValue());
        $fbUser = $response->getGraphUser();

        $user = $this->userProvider->loadUserByEmail($fbUser['email']);

        if ($user === null)
        {
            $this->logger->debug('Attempting to register user with their facebook', ['facebook_user' => $fbUser]);
            $user = new User();
            $now = new \DateTime();
            $user
                ->setDateCreated($now)
                ->setDateUpdated($now)
                ->setUsername($this->parseFacebookName($fbUser['first_name']))
                ->setPlainPassword(null)
                ->setEmail($fbUser['email'])
                ->setGender(User::GENDER_UNKNOWN);

            try {
                $this->userManager->registerWithFacebook($user);
            } catch (UserRegisterException $e) {
                return new Response($e->getMessage(), 400);
            }
            $this->entityManager->flush();
        }

        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
        $this->tokenStorage->setToken($token);

        return new Response('', 200);
    }
}
