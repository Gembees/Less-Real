<?php

namespace UserBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;
use UserBundle\Exception\UserRegisterException;
use UserBundle\Service\UserManager;

class RegisterController extends Controller
{
    const ERROR_MESSAGE_USERNAME_ALREADY_REGISTERED = "Username already in use";
    const ERROR_MESSAGE_EMAIL_ALREADY_REGISTERED = "Email already in use";
    const ERROR_MESSAGE_PASSWORD_EMPTY = "Password cannot be empty";

    private $entityManager;
    private $twig;
    private $formFactory;
    private $userManager;
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        TwigEngine $twig,
        FormFactoryInterface $formFactory,
        UserManager $userManager,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
        $this->logger = $logger;
    }

    public function register()
    {
        $form = $this->formFactory->createBuilder()
            ->add('username', TextType::class, [
                'attr' => [
                    'placeholder' => 'Username',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'Email'
                ]
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'Password'
                ]
            ])
            ->add('submit', SubmitType::class)
            ->getForm();


        return $this->twig->renderResponse('UserBundle:Register:register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function check(Request $request)
    {
        $this->logger->debug('Attempting to register user', ['request' => $request->request]);
        $user = new User();
        $now = new \DateTime();
        $user
            ->setDateCreated($now)
            ->setDateUpdated($now)
            ->setUsername($request->request->get('username'))
            ->setPlainPassword($request->request->get('password'))
            ->setEmail($request->request->get('email'))
            ->setGender(User::GENDER_UNKNOWN);

        try {
            $this->userManager->registerWithForm($user);
        } catch (UserRegisterException $e) {
            return new Response($e->getMessage(), 400);
        }
        $this->entityManager->flush();

        return new Response(json_encode($user->getUsername()), 201);
    }
}
