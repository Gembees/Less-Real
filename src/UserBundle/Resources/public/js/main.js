$('#continue_with_facebook').click(function(e) {
    modalLoading(true);
    FB.login(function(response) {
        if (response.authResponse) {
            $.post(uri('login_facebook'), function(response, textStatus, xhr) {
                console.log('RESPONSE: ' + response);
                if (xhr.status === 200) {
                    window.location = uri();
                    modalLoading(false);
                } else {
                    //TODO: something went wrong
                }
            });
        } else {
            modalLoading(false);
        }
    }, {scope: 'email'});
    e.preventDefault();
});