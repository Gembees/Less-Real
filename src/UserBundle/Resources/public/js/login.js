$(document).ready(function() {
    var fields = [
        'username',
        'password'
    ];

    $('#login_form :input').on('input', function() {
        checkSubmit('login', fields);
    });

    $('#login_submit').click(function(e) {
        modalLoading(true);

        var username = $('#login_username').val();
        var password = $('#login_password').val();
        $.ajax({
            method: 'POST',
            data: {
                _username: username,
                _password: password
            },
            url: uri("login_check"),
            success: function (response) {
                console.log(response);
                window.location = uri();
            },
            statusCode: {
                401: function (response) {
                    var responseText = JSON.parse(response.responseText);
                    $("#login_form_errors").hide();
                    if (responseText.error.length > 0) {
                        $("#login_form_errors").show().text(responseText.error);
                    }
                    modalLoading(false);
                }
            },
            error: function (e) {

            }
        });
        e.preventDefault();
    });
});