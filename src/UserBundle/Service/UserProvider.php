<?php

namespace UserBundle\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class UserProvider implements UserProviderInterface
{
    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     * @return null|User
     */
    public function loadUserByUsername($username)
    {
        return $this->userRepository->findOne([
                'username' => $username
        ]);
    }

    /**
     * @param string $email
     * @return null|User
     */
    public function loadUserByEmail(string $email)
    {
        return $this->userRepository->findOne([
            'email' => $email
        ]);
    }

    /**
     * @param UserInterface $user
     * @return null|User
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->userRepository->findOneById($user->getId());
    }

    public function supportsClass($class)
    {
        //TODO: Implement supportsClass() method.
    }
}
