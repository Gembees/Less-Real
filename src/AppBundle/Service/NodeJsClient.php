<?php

namespace AppBundle\Service;

use GuzzleHttp\Client;
use JMS\Serializer\Serializer;

class NodeJsClient
{
    private $serializer;
    private $guzzle;

    public function __construct(
        Serializer $serializer
    ) {
        $this->serializer = $serializer;
        $this->guzzle = new Client([
            'base_uri' => 'http://localhost:8000'
        ]);
    }

    /**
     * @param $eventName
     * @param $event
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function dispatchEvent($eventName, $event)
    {
        $response = $this->guzzle->request(
            'POST',
            'http://localhost:8000/image',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode([
                    'eventName' => $eventName,
                    'event' => $this->serializer->serialize($event, 'json')
                ])
            ]
        );

        return $response;
    }
}
