<?php

namespace AppBundle\Service;

class ImageManipulator
{
    /**
     * @param integer $newWidth
     * @param integer $newHeight
     * @param Resource $image
     * @return Resource
     */
    public function cropImage($newWidth, $newHeight, $image)
    {
        $width = imagesx($image);
        $height = imagesy($image);

        $originalAspect = $width / $height;
        $newAspect = $newWidth / $newHeight;

        if ($originalAspect >= $newAspect) {
            // If image is wider than thumbnail (in aspect ratio sense)
            $finalHeight = $newHeight;
            $finalWidth = $width / ($height / $newHeight);
        } else {
            // If the thumbnail is wider than the image
            $finalWidth = $newWidth;
            $finalHeight = $height / ($width / $newWidth);
        }

        $thumb = imagecreatetruecolor($newWidth, $newHeight);

        // Resize and crop
        imagecopyresampled(
            $thumb,
            $image,
            0 - ($finalWidth - $newWidth) / 2, // Center the image horizontally
            0 - ($finalHeight - $newHeight) / 2, // Center the image vertically
            0,
            0,
            $finalWidth,
            $finalHeight,
            $width,
            $height
        );

        return $thumb;
    }

    /**
     * @param Resource $image
     * @param integer $width
     * @param integer $height
     * @param integer $x1
     * @param integer $y1
     * @param integer $x2
     * @param integer $y2
     * @return Resource
     */
    public function cropImageAdvanced(
        $image,
        $width,
        $height,
        $x1 = 0,
        $y1 = 0,
        $x2 = 0,
        $y2 = 0
    ) {
        $trueColorImage = imagecreatetruecolor($width, $height);

        imagecopyresampled(
            $trueColorImage,
            $image,
            0,
            0,
            $x1,
            $y1,
            $width,
            $height,
            floor($x2 - $x1),
            floor($y2 - $y1)
        );

        return $trueColorImage;
    }

    /**
     * @param Resource $image
     * @param integer $width
     * @param integer $height
     * @param integer $maxWidth
     * @param integer $maxHeight
     * @return Resource
     */
    public function resizeImage(
        $image,
        $width,
        $height,
        $maxWidth,
        $maxHeight
    ) {
        if ($maxWidth >= $width && $maxHeight >= $height) {
            return $image;
        }

        $proportions = $width / $height;

        if ($width > $height) {
            $newWidth = $maxWidth;
            $newHeight = ceil($maxWidth / $proportions);
        } else {
            $newWidth = $maxWidth;
            $newHeight = ceil($maxHeight * $proportions);
        }

        $trueColorImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($trueColorImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        return $trueColorImage;
    }

    /**
     * @param string $path
     * @return resource
     * @throws \ErrorException
     */
    public function ConvertPngToJpg($path)
    {
        if (!$pngImage = imagecreatefrompng($path)) {
            throw new \ErrorException('Could not load png image.');
        }
        $trueColorImage = imagecreatetruecolor(imagesx($pngImage), imagesy($pngImage));
        imagefill($trueColorImage, 0, 0, imagecolorallocate($trueColorImage, 255, 255, 255));
        imagealphablending($trueColorImage, true);
        imagecopy($trueColorImage, $pngImage, 0, 0, 0, 0, imagesx($pngImage), imagesy($pngImage));
        imagedestroy($pngImage);
        $quality = 100; // 0 = worst / smaller file, 100 = better / bigger file
        imagejpeg($trueColorImage, $path . ".jpg", $quality);
        imagedestroy($trueColorImage);

        if (!$jpgImage = imagecreatefromjpeg($path . ".jpg")) {
            throw new \ErrorException('Could not convert png image to jpg.');
        }

        return $jpgImage;
    }
}
