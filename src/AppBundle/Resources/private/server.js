var express = require('../../../../vendor/node_modules/express');
var http = require('http');
var socket = require('../../../../vendor/node_modules/socket.io');
var bodyParser = require('../../../../vendor/node_modules/body-parser');

var app = express();
app.use(bodyParser.json());
var server = http.createServer(app);
var io = socket.listen(server);
io.sockets.on('connection', function(client, next) {
    console.log("New client!");

    app.post("/image", function (request, response) {
        if (request.method == 'POST') {
            var eventName = request.body.eventName;
            var event = JSON.parse(request.body.event);
            console.log('Event caught: ' + eventName);

            if (eventName === 'image.uploaded') {
                console.log('Initiated by: ' + event.image.user);
                console.log('id: ' + event.image.id);
                io.emit('image.uploaded', {
                    id: event.image.id
                });
            }

            response.set('Content-Type', 'text/plain');
            response.send(eventName);
        }
    });

    app.get("/autocomplete", function (request, response) {
        if (request.method == 'GET') {
            console.log(request.query);


            response.set('Content-Type', 'text/plain');
            response.send('hey');
        }
    });

    client.on('imageupload', function() {
        //console.log("Image upload");
        client.emit('imageupload');
    });

    client.on('image_uploaded', function() {
        //console.log("Image very much uploaded!");
    });
});

server.listen(8000);
console.log("Listening to 8000");

