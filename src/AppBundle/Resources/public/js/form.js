(function ( $ ) {

    $.fn.simplete = function( options ) {

        var cache = [];
        var self = this;
        var queryLength = 0;
        var settings = $.extend({
            color: "#556b2f",
            backgroundColor: "white",
            showResults: 10,
            minChar: 1
        }, options );

        $(document).keyup(function(e) {
            if (e.keyCode === 13) {
                //enter
                hideHints();
            }
            if (e.keyCode === 27) {
                //esc
                hideHints();
            }
        });

        $(document).click(function() {
            hideHints();
        });

        this.click(function(e) {
            populateAutocomplete();
            e.stopPropagation();
        });

        this.keyup(function() {
            populateAutocomplete();
        });

        function hideHints()
        {
            $('.simplete').remove();
        }

        function populateAutocomplete()
        {
            var q = self.val();
            var lastQueryLength = queryLength;
            queryLength = q.length;
            if (q.length < settings.minChar) {
                hideHints();
                return false;
            }

            var cachedData = findInCache(q);
            if (cachedData !== false /*&& queryLength > lastQueryLength*/) {
                displayData(q, cachedData);
            } else {
                displaySearching();
                $.ajax({
                    method: 'GET',
                    url: uri('api/label/search'),
                    data: {q: q},
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data.length > 0) {
                            pushToCache(data);
                            displayData(q, data);
                        } else {
                            displayNothingFound();
                        }
                    }
                });
            }
        }

        function pushToCache(data)
        {
            for (var j = 0; j < data.length; j++) {
                var push = true;
                for (var i = 0; i < cache.length; i++) {
                    if (data[j].value.toLowerCase() == cache[i].value.toLowerCase()) {
                        push = false;
                    }
                }
                if (push) {
                    cache.push(data[j]);
                }
            }
        }

        function highlightKeyword(q, value)
        {
            var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + q + ")(?![^<>]*>)(?![^&;]+;)", "gi") ;
            return value.replace(re, "<span class='simplete-highlight'>$1</span>");
        }

        function findInCache(q)
        {
            var tags = [];
            for (var i = 0; i < cache.length; i++) {
                if (cache[i].value.toLowerCase().match(new RegExp(q.toLowerCase() + '.*', 'i'))) {
                    tags.push(cache[i]);
                }

                if (tags.length >= settings.showResults) {
                    return tags;
                }
            }

            if (tags.length > 0) {
                return tags;
            } else {
                return false;
            }
        }

        function displayData(q, data)
        {
            hideHints();
            var insertData = '';
            for (var i = 0; i < data.length; i++) {
                var tempValue = highlightKeyword(q, data[i].value);
                insertData = insertData + "<li class='item' data-label-id='"+data[i].id+"'>" + tempValue + "</li>";
            }
            self.parent().append('<div class="simplete"><ul>' + insertData + '</ul></div>');

            $('.simplete').children().children('.item').click(function() {
                self.val($(this).text());
                self.focus();
                $.ajax({
                    method: 'POST',
                    url: uri('api/image/tag/' + $(this).attr('data-label-id')),
                    data: { imageIds: JSON.stringify(getImageIds(imageIds)) },
                    success: function(response) {
                        updateTags('images', imageIds);
                        self.val('');

                    }
                });
            });
        }

        function displayNothingFound()
        {
            hideHints();
            self.parent().append('<div class="simplete"><ul style="padding-left: 8px;">No results...</ul></div>');
        }

        function displaySearching()
        {
            hideHints();
            self.parent().append('<div class="simplete"><ul style="padding-left: 8px;">Searching...</ul></div>');
        }

        return this.css({
            color: settings.color,
            backgroundColor: settings.backgroundColor
        });
    };
}( jQuery ));

var helpyFaceImgUrl = 'http://yoursmiles.org/ismile/anime/azumangadaioh4.png';
var helpyFace = "<img src='" + helpyFaceImgUrl + "' class='ui middle aligned tiny image' style='margin-right: 1px;' />";

var formVisualResponse = {
    prefix: 'form_',
    fields: [],
    fieldsValid: [],
    total: 0,
    init: function(prefix, fields) {
        this.prefix = prefix + '_';
        this.fields = fields;
        this.total = fields.length;
        for (var i = 0; i < this.fields.length; i++) {
            this.checkField(this.fields[i]);
        }
    },
    markFieldAsValid: function(field) {
        if ($.inArray(field, this.fieldsValid) === -1) {
            this.fieldsValid.push(field);
        }
    },
    markFieldAsInvalid: function(field) {
        if ($.inArray(field, this.fieldsValid) !== -1) {
            this.fieldsValid.splice($.inArray(field, this.fieldsValid), 1);
        }
    },
    checkField: function(field) {
        var _this = this;
        $('#' + _this.prefix + field).on('input', function() {
            if ($('#' + _this.prefix + 'form').form('is valid', field)) {
                $(this).parent().parent().addClass('success');
                $(this).parent().children('i').remove();
                $(this).parent().append('<i class="check green large icon"></i>');
                _this.markFieldAsValid(field);
            } else {
                $(this).parent().parent().removeClass('success');
                $(this).parent().children('i').remove();
                _this.markFieldAsInvalid(field);
            }
            _this.checkSubmit();
        });
    },
    checkSubmit: function() {
        if (this.fieldsValid.length < this.total) {
            $('#' + this.prefix + 'submit').removeClass('green').addClass('disabled');
        } else {
            $('#' + this.prefix + 'submit').removeClass('disabled').addClass('green');
        }
    }
};

function checkSubmit(prefix, fields)
{
    var valid = true;
    for (var i = 0; i < fields.length; i++) {
        if (!$('#' + prefix + '_' + fields[i]).val()) {
            valid = false;
        }
    }
    if (!valid) {
        $('#' + prefix + '_' + 'submit').removeClass('green').addClass('disabled');
    } else {
        $('#' + prefix + '_' + 'submit').removeClass('disabled').addClass('green');
    }
}

function formIsSubmitted()
{
    return $('#form').data('submitted') === true
}

function markFormAsSubmitted(flag)
{
    if (flag === undefined || flag === true) {
        $('#form').data('submitted', true);
        $('#form_submit').addClass('loading active');
    } else if (flag === false) {
        $('#form').data('submitted', false);
        $('#form_submit').removeClass('loading active');
    }
}