<?php

namespace QuoteBundle\Controller;

use QuoteBundle\Entity\Quote;
use UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Mapping as ORM;

class ApiController extends Controller
{
    public function getSingleQuoteAction($id)
    {
        $quote = $this->getDoctrine()
            ->getRepository('QuoteBundle:Quote')
            ->find($id);

        if (!$quote) {
            throw $this->createNotFoundException('asdasd');
        }

        $responseData = [
            'id' => $quote->getId(),
            'body' => $quote->getBody(),
            'userId' => $quote->getUserId(),
            'characterId' => $quote->getCharacterId(),
            'sourceId' => $quote->getSourceId(),
            'dateCreated' => $quote->getDateCreated(),
            'dateUpdated' => $quote->getDateUpdated(),
        ];

        $response = new Response(json_encode($responseData), 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function updateSingleQuoteAction(Request $request, $id)
    {
        $quoteData = json_decode($request->getContent(), true);

        if (!$this->authorizeAction($quoteData['token']))
        {
            throw $this->createNotFoundException('Invalid token.');
        }

        $quote = $this->getDoctrine()
            ->getRepository('QuoteBundle:Quote')
            ->find($id);

        if (!$quote) {
            throw $this->createNotFoundException('asdasd');
        }

        $now = new \DateTime();
        $quote
            ->setBody($quoteData['body'])
            ->setCharacterId($quoteData['characterId'])
            ->setUserId($quoteData['userId'])
            ->setSourceId($quoteData['sourceId'])
            ->setDateUpdated($now);

        $em = $this->getDoctrine()->getManager();
        $em->persist($quote);
        $em->flush();

        $responseData = [
            'id' => $quote->getId(),
            'body' => $quote->getBody(),
            'userId' => $quote->getUserId(),
            'characterId' => $quote->getCharacterId(),
            'sourceId' => $quote->getSourceId(),
            'dateCreated' => $quote->getDateCreated(),
            'dateUpdated' => $quote->getDateUpdated(),
        ];

        $response = new Response(json_encode($responseData), 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function insertSingleQuoteAction(Request $request)
    {
        $quoteData = json_decode($request->getContent(), true);

        $now = new \DateTime();
        $quote = new Quote();
        $quote
            ->setBody($quoteData['body'])
            ->setCharacterId($quoteData['characterId'])
            ->setUserId($quoteData['userId'])
            ->setSourceId($quoteData['sourceId'])
            ->setDateUpdated($now)
            ->setDateCreated($now);

        $em = $this->getDoctrine()->getManager();

        $em->persist($quote);
        $em->flush();

        $responseData = [
            'id' => $quote->getId(),
            'body' => $quote->getBody(),
            'userId' => $quote->getUserId(),
            'characterId' => $quote->getCharacterId(),
            'sourceId' => $quote->getSourceId(),
            'dateCreated' => $quote->getDateCreated(),
            'dateUpdated' => $quote->getDateUpdated(),
        ];

        $response = new Response(json_encode($responseData), 201);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
