<?php

namespace LabelBundle\Entity;

use UserBundle\Entity\User;

class Label
{
    const LABEL_TYPE_CHARACTER = 4;
    const LABEL_TYPE_ANIME = 2;

    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $verified;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $totalEntitiesTagged;

    /**
     * @var \DateTime
     */
    private $dateUpdated;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    public function __construct()
    {
        $this->verified = false;
        $this->totalEntitiesTagged = 0;
        $this->dateCreated = new \DateTime();
        $this->dateUpdated = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): Label
    {
        $this->verified = $verified;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): Label
    {
        $this->type = $type;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): Label
    {
        $this->text = $text;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): Label
    {
        $this->description = $description;
        return $this;
    }

    public function getTotalEntitiesTagged(): int
    {
        return $this->totalEntitiesTagged;
    }

    public function setTotalEntitiesTagged(int $totalEntitiesTagged): Label
    {
        $this->totalEntitiesTagged = $totalEntitiesTagged;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Label
    {
        $this->user = $user;
        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTime $dateUpdated): Label
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTime $dateCreated): Label
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }
}
