<?php

namespace LabelBundle\Controller;

use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use LabelBundle\Entity\LabelTag;
use LabelBundle\Entity\Label;
use LabelBundle\Repository\LabelRepository;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    private $entityManager;
    private $serializer;
    private $twig;
    private $labelRepository;
    private $logger;

    public function __construct(
        EntityManager $entityManager,
        Serializer $serializer,
        TwigEngine $twig,
        LabelRepository $labelRepository,
        Logger $logger
    ) {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->twig = $twig;
        $this->labelRepository = $labelRepository;
        $this->logger = $logger;
    }

    public function search(Request $request)
    {
        $q = $request->query->get('q');

        $labels = $this->labelRepository
            ->getSuggestionsForAutocomplete($q);

        $returnLabels = [];
        /** @var Label $label */
        foreach ($labels as $label) {
            $returnLabels[] = [
                'value' => $label->getText(),
                'id' => $label->getId()
            ];
            $this->logger->debug('Label: ' . $label->getText() .'end');
            $this->logger->debug('Id: ' . $label->getId() . 'end');
        }

        return new Response(json_encode($returnLabels), 200);
    }

    public function getEntityTags($objectEntity, $objectId)
    {
        $objectTags = $this->entityManager->getRepository('LabelBundle:ObjectsTags')
            ->findBy([
                'objectTable' => $objectEntity,
                'objectId' => $objectId
            ]);

        $primaryTags = [];
        /** @var LabelTag $objectTag */
        foreach ($objectTags as $objectTag) {
            $primaryTags[] = $this->entityManager->getRepository('LabelBundle:PrimaryTags')
                ->find($objectTag->getLabel());
        }

        return $primaryTags;
    }

    private function isObjectTaggedWith($tagId, $objectType, $objectId)
    {
        $tag = $this->entityManager->getRepository('LabelBundle:ObjectsTags')
            ->findOneBy([
                'tagId' => $tagId,
                'objectTable' => $objectType,
                'objectId' => $objectId
            ]);
        if ($tag !== null) {
            return $tag;
        } else {
            return false;
        }
    }

    public function tagEntity(Request $request, $tagId, $objectType)
    {
        $imageIds = json_decode($request->request->get('imageIds'));

        foreach ($imageIds as $imageId) {
            if ($this->isObjectTaggedWith($tagId, $objectType, $imageId)) {
                continue;
                //throw new \Exception('Object already tagged with this tag.');
            }
            $objectTag = new LabelTag();
            $objectTag->setUserId(1);
            $objectTag->setLabel($tagId);
            $objectTag->setObjectTable($objectType);
            $objectTag->setObjectId($imageId);
            $objectTag->setDateUpdated('2017-02-11');
            $objectTag->setDateCreated('2017-02-11');

            $this->entityManager->persist($objectTag);
        }

        $this->entityManager->flush();

        return new Response('', 201);
    }

    public function untagEntity(Request $request, $tagId, $objectType)
    {
        $imageIds = json_decode($request->request->get('imageIds'));
        foreach ($imageIds as $imageId) {
            if (!$objectTag = $this->isObjectTaggedWith($tagId, $objectType, $imageId)) {
                throw new \Exception('Object not tagged with this tag.');
            }
            $this->entityManager->remove($objectTag);
        }

        $this->entityManager->flush();

        return new Response('', 200);
    }
}
