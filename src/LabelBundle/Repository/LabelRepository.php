<?php

namespace LabelBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use LabelBundle\Entity\Label;

class LabelRepository extends EntityRepository
{
    public function findOneById(int $id): ?Label
    {
        return $this->find($id);
    }

    /**
     * @param int $n
     * @return array Label|null
     */
    public function getNewestLabels(int $n): ?array
    {
        //TODO: actually fetch unfinished image uploads
        return $this->createQueryBuilder('t')
            ->orderBy('t.dateCreated', 'DESC')
            ->setMaxResults($n)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $searchKey
     * @return array Label|null
     */
    public function getSuggestionsForAutocomplete(string $searchKey): ?array
    {
        $rsm = (new ResultSetMapping())
            ->addEntityResult(Label::class, 'l')
            ->addFieldResult('l', 'id', 'id')
            ->addFieldResult('l', 'text', 'text')
        ;

        return $this->getEntityManager()->createNativeQuery('
              SELECT l.id, l.text FROM label l
              WHERE l.text LIKE :searchKey
              ORDER BY (l.text LIKE :searchKey) DESC, l.total_entities_tagged DESC
              LIMIT 10
        ', $rsm)
            ->setParameter('searchKey', '%'.$searchKey.'%')
            ->getResult();
    }
}
