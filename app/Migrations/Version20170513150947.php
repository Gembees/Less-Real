<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170513150947 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE image_label_tag (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, label_id INT DEFAULT NULL, image_id INT DEFAULT NULL, date_updated DATETIME NOT NULL, date_created DATETIME NOT NULL, UNIQUE INDEX UNIQ_EFE93563A76ED395 (user_id), UNIQUE INDEX UNIQ_EFE9356333B92F39 (label_id), UNIQUE INDEX UNIQ_EFE935633DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image_label_tag ADD CONSTRAINT FK_EFE93563A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE image_label_tag ADD CONSTRAINT FK_EFE9356333B92F39 FOREIGN KEY (label_id) REFERENCES label (id)');
        $this->addSql('ALTER TABLE image_label_tag ADD CONSTRAINT FK_EFE935633DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image_label_tag DROP FOREIGN KEY FK_EFE93563A76ED395');
        $this->addSql('ALTER TABLE image_label_tag DROP FOREIGN KEY FK_EFE9356333B92F39');
        $this->addSql('ALTER TABLE image_label_tag DROP FOREIGN KEY FK_EFE935633DA5256D');
        $this->addSql('DROP TABLE image_label_tag');
    }
}
