<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170514193431 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image_label_tag DROP INDEX UNIQ_EFE93563A76ED395, ADD INDEX IDX_EFE93563A76ED395 (user_id)');
        $this->addSql('ALTER TABLE image_label_tag DROP INDEX UNIQ_EFE9356333B92F39, ADD INDEX IDX_EFE9356333B92F39 (label_id)');
        $this->addSql('ALTER TABLE image_label_tag DROP INDEX UNIQ_EFE935633DA5256D, ADD INDEX IDX_EFE935633DA5256D (image_id)');
        $this->addSql('ALTER TABLE image DROP INDEX UNIQ_C53D045FA76ED395, ADD INDEX IDX_C53D045FA76ED395 (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image DROP INDEX IDX_C53D045FA76ED395, ADD UNIQUE INDEX UNIQ_C53D045FA76ED395 (user_id)');
        $this->addSql('ALTER TABLE image_label_tag DROP INDEX IDX_EFE9356333B92F39, ADD UNIQUE INDEX UNIQ_EFE9356333B92F39 (label_id)');
        $this->addSql('ALTER TABLE image_label_tag DROP INDEX IDX_EFE935633DA5256D, ADD UNIQUE INDEX UNIQ_EFE935633DA5256D (image_id)');
        $this->addSql('ALTER TABLE image_label_tag DROP INDEX IDX_EFE93563A76ED395, ADD UNIQUE INDEX UNIQ_EFE93563A76ED395 (user_id)');
    }
}
