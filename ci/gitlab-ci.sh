export LANG=C.UTF-8
apt-get -y update
apt-get -y install software-properties-common
add-apt-repository -y ppa:ondrej/php
apt-get update -y
apt-get -y install curl git php7.1 php7.1-mysql php7.1-curl php7.1-gd php7.1-dom php7.1-mbstring libapache2-mod-php7.1 zip unzip php7.1-zip composer nodejs npm
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install mysql-server
service mysql start
mysql --user="root" --password="" --execute="CREATE DATABASE lessreal; CREATE USER lessreal@localhost IDENTIFIED BY 'lessreal'; GRANT ALL PRIVILEGES ON lessreal.* TO lessreal@localhost; FLUSH PRIVILEGES;"
