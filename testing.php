<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16.10.29
 * Time: 20.03
 */

require __DIR__.'/vendor/autoload.php';

$client = new \GuzzleHttp\Client([
    'base_url' => 'http://symfony.dev:80',
    'defaults' => [
        'exceptions' => false,
    ]
]);

$nickname = 'ObjectOrienter'.rand(0,999);
$data = [
    'body' => 'OR DID HE?! lived happily ever after!',
    'userId' => '23232',
    'sourceId' => '0',
    'characterId' => '0',
];

//$response = $client->get('http://symfony.dev/api/quotes/1555');
$response = $client->post('http://symfony.dev/api/quote/2', [
   'body' => json_encode($data)
]);

echo $response->getBody();
echo "\n\n";
